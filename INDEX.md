# E1000PKT

Packet driver for Intel(R) PRO/1000 adapters, for instance 82544, 82540, 82545, 82541, and 82547 based Ethernet controllers.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## E1000PKT.LSM

<table>
<tr><td>title</td><td>E1000PKT</td></tr>
<tr><td>version</td><td>February 1, 2007 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-08-24</td></tr>
<tr><td>description</td><td>Packet driver for Intel(R) PRO/1000</td></tr>
<tr><td>summary</td><td>Packet driver for Intel(R) PRO/1000 adapters, for instance 82544, 82540, 82545, 82541, and 82547 based Ethernet controllers.</td></tr>
<tr><td>keywords</td><td>82544, 82540, 82545, 82541, 82547, GIGPKTDRVR, Gigabit Packet Driver</td></tr>
<tr><td>author</td><td>Intel Corporation</td></tr>
<tr><td>maintained&nbsp;by</td><td>my.green@mailbox.org</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/ulrich-hansen/E1000PKT</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://github.com/ulrich-hansen/E1000PKT</td></tr>
<tr><td>original&nbsp;site</td><td>https://downloadcenter.intel.com/download/12586</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2.</td></tr>
</table>
